const findNumbers = (str) => {
  for(var i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) < 48 || str.charCodeAt(i) > 57) {
      return false;
    }
  }
  return true;
}

const getSum = (str1, str2) => {
  if ((typeof str1 == "string" || str1 instanceof String) && (typeof str2 === "string" || str2 instanceof String)) {
    if (findNumbers(str1) && findNumbers(str2)) {
      var res = '';
      var s1, s2;
      if (str1.length > str2.length) {
        s1 = str2;
        s2 = str1;
      } else {
        s1 = str1;
        s2 = str2;
      }
      var r = 0;
      for (var i = 0; i < s1.length; i++) {
        var n1 = Number(s1[s1.length - 1 - i]);
        var n2 = Number(s2[s2.length - 1 - i]);
        r = parseInt((n1 + n2 + r) / 10);
        res = String((n1 + n2 + r) % 10) + res;
      }
      for (var i = s1.length; i < s2.length; i++) {
        res = s2[i] + res;
      }
      return res;
    }
    return false;
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var res = 'Post:';
  var pres = 0;
  var cres = 0;
  for (var i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i]['author'] == authorName) {
      pres++;
    }
    if ('comments' in listOfPosts[i]) {
      for (var j = 0; j < listOfPosts[i]['comments'].length; j++) {
        if (listOfPosts[i]['comments'][j]['author'] == authorName) {
          cres++;
        }
      }
    }
  }
  res += pres + ',comments:' + cres;
  return res;
};

const tickets=(people)=> {
  var sum = 0;
  for (var i = 0; i < people.length; i++) {
    if (people[i] == 25) {
      sum += people[i];
    } else {
      if (sum - people[i] + 25 < 0) {
        return 'NO';
      }
      sum = sum - (people[i] - 25) + 25;
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
